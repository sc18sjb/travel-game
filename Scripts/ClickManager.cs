﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickManager : MonoBehaviour
{
    public static Person personBuffer;
    public static Node nodeBuffer;

    public static void pollStates()
    {
        Debug.Log("Polling states");
        Person pB = ClickManager.personBuffer;
        Node nB = ClickManager.nodeBuffer;

        if(nodeBuffer == null && personBuffer == null)
            return;

        if(personBuffer.getCurrent().getTo() != null) 
        {
            if(personBuffer.getCurrent().getTo().gameObject.GetInstanceID() != nodeBuffer.gameObject.GetInstanceID())
            {
                Person.Status s = personBuffer.getStatus();
                if(s == Person.Status.Walking || s == Person.Status.Unassigned || s == Person.Status.Waiting)
                {
                    if(Vector3.Distance(pB.transform.position, nB.transform.position) < 15)
                    {
                        //We've told the player to go somewhere
                        ClickManager.GoToNode(nB, pB);
                    }
                    
                }
            }
            else
            {
                if(personBuffer.getStatus() == Person.Status.Unassigned)
                {
                    personBuffer.setStatus(Person.Status.Waiting);
                }
                
            }
        }
        else
        {
            Person.Status s = personBuffer.getStatus();
            if(s == Person.Status.Walking || s == Person.Status.Unassigned || s == Person.Status.Waiting)
            {
                //We've told the player to go somewhere
                if(Vector3.Distance(pB.transform.position, nB.transform.position) < 10)
                    ClickManager.GoToNode(nB, pB);
            }
        }


        ClickManager.personBuffer = null;
        ClickManager.nodeBuffer = null;
    }

    public static void GoToNode(Node n, Person p)
    {
        Debug.Log("GOING SOMEWHERE");
        Connection tmpConnection = new Connection(p.getCurrent().getTo(), nodeBuffer, 0, (Type)2, 0, 0);
        if(tmpConnection.getFrom() != null)
        {
            tmpConnection.getFrom().removePerson(p);
        }
        p.setConnection(tmpConnection);
        p.setStatus(Person.Status.Walking);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
