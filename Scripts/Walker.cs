﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walker : MonoBehaviour
{
    public enum Status
    {
        WAITING,
        MOVING
    }

    [SerializeField]
    protected List<Person> people = new List<Person>();

    [SerializeField]
    protected float speed = 0;

    [SerializeField]
    protected float progress = 0;

    [SerializeField]
    protected Connection current = new Connection(null, null, 0, (Type)0, 0, 0);

    [SerializeField]
    protected Type type;

    private Walker.Status status;

    // Start is called before the first frame update
    public virtual void init(float speed, Connection current)
    {
        this.setSpeed(speed);
        this.setConnection(current);
        this.setStatus(Walker.Status.WAITING);
        TimeManager.OnTick += this.tick;
    }


    // setters

    public virtual void tick()
    {
        progress += 0.005f * speed;
        if (progress > current.getLength()) {
            progress = current.getLength();
        }
    }

    protected void setSpeed(float speed)
    {
        this.speed = speed;
    }

    protected void setConnection(Connection current)
    {
        this.current = current;
    }

    protected void setStatus(Walker.Status status)
    {
        this.status = status;
    }

    protected void setType(Type type) {
        this.type = type;
    }


    // getters

    public float getSpeed()
    {
        return this.speed;
    }
    
    public List<Person> getPeople()
    {
        return this.people;
    }


    public void addPerson(Person p)
    {
        this.people.Add(p);
    }

    public void removePerson(Person p)
    {
        this.people.Remove(p);
    }

    public Connection getConnection()
    {
        return this.current;
    }

    public float getProgress()
    {
        return progress;
    }

    public Type getType()
    {
        return type;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
