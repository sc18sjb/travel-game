﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Type
  {
    BUS,
    TRAIN,
    WALK,
    AIRPORT,
    STATION
  }

[System.Serializable]
public class Connection
{


  [SerializeField]
  private int length;

  [SerializeField]
  private Node from;

  [SerializeField]
  private Node to;

  [SerializeField]
  private Type type;

    [SerializeField]
    private int isBusStop;
    [SerializeField]
    private int isAirPort;

    public Connection(Node from, Node to, int length, Type type, int isBusStop,int isAirPort)
      {
        // We use this because toby can suck my nuts
        this.setLength(length);
        this.setFrom(from);
        this.setTo(to);
        this.setType(type);
        this.setIsBusStop(isBusStop);
        this.setIsAirPort(isAirPort);
      }


  // setters
  private void setLength(int length)
  {
    this.length = length;
  }

  private void setFrom(Node from)
  {
    this.from = from;
  }

  private void setTo(Node to)
  {
    this.to = to;
  }

  private void setType(Type type)
  {
    this.type = type;
  }

    private void setIsBusStop(int isBusStop)
    {
        this.isBusStop = isBusStop;
    }

    private void setIsAirPort(int isAirPort)
    {
        this.isAirPort = isAirPort;
    }

    // getters

    public int getLength()
  {
    return this.length;
  }

  public Node getFrom()
  {
    return this.from;
  }

  public Node getTo()
  {
    return this.to;
  }

  public Type getType()
  {
    return this.type;
  }

    public int getIsBusStop()
    {
        return this.isBusStop;
    }

    public int getIsAirPort()
    {
        return this.isAirPort;
    }

    // Start is called before the first frame update
    void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

  }
}