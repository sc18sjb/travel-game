﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickSpriteMessage : MonoBehaviour
{
    [SerializeField]
    private string message;

    private bool mouseOver;

    private void OnMouseOver()
    {
        mouseOver = true;
    }

    private void OnMouseExit()
    {
        mouseOver = false;
    }

    private void OnMouseDown()
    {
        if(mouseOver)
        {
            SendMessage(message, SendMessageOptions.DontRequireReceiver);
        }
    }

    public void setMessage(string msg)
    {
        message = msg;
    }

    public bool isHovered()
    {
        return mouseOver;
    }
}
