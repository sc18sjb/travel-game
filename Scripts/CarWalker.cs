﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarWalker : Walker { 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Constructor
    public void init(string type, int speed, Connection current, Person passenger)
    {
        this.setSpeed(speed);
        this.setConnection(current);
        this.setPassenger(passenger); // add the passenger to the car on construction of the car walker
        TimeManager.OnTick += this.tick;
    }

    // setters 
    private void setPassenger(Person passenger)
    {
        this.addPerson(passenger);
    }



    // Update is called once per frame
    void Update()
    {
        
    }
}
