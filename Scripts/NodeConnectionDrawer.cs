﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeConnectionDrawer : MonoBehaviour
{
    Node node;

    [SerializeField]
    private Color connectionColour;

    [SerializeField]
    private float connectionThickness;

    // Start is called before the first frame update
    void Start()
    {
        node = GetComponent<Node>();

        List<Connection> connections = node.getConnections();

        foreach(Connection con in connections)
        {
            DrawConnection.CreateNewLineGraphic(connectionColour, connectionThickness, con);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
