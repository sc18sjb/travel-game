﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour
{
    
    public enum Status
    {
    Waiting,
    Moving,
    Unassigned,
    Departing,
    Walking
    }

    public static int counter; // number of people in the game
    private int id;
    private string personName;
    private string type; //priority
    private int start;

    [SerializeField]
    private Connection current;

    private float walkSpeed = 2.5f;

    private Person.Status status;

    Color hoverColour = Color.grey;
    Color standardColour;
    SpriteRenderer spr;

    private float remainingTime = 75;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, 1);
        spr = GetComponent<SpriteRenderer>();
        spr.sortingOrder = 3;
        if(spr != null)
            standardColour = spr.color;

        gameObject.AddComponent<BoxCollider2D>();
        Person.counter += 1;
        TimeManager.OnTick += tick;

        OnClickSpriteMessage clicker = gameObject.AddComponent<OnClickSpriteMessage>();
        clicker.setMessage("PressPlayer");
    }

    private void OnMouseOver()
    {
        spr.color = hoverColour;
    }

    private void OnMouseExit()
    {
        spr.color = standardColour;
    }

    public int getTimeRemain()
    {
        return Mathf.RoundToInt(Mathf.Clamp(remainingTime, 0, Mathf.Infinity));
    }

    public void tick()
    {
        // If a person is at a node marked at wait (so its a bus stop), then add the person to the nodes people list
        // when a bus gets to that node, if there is anyone within the people list, pick them all up and set their status to moving
        try
        {
            if(status == Status.Walking)
                    {
           
                        if (current.getTo() != null)
                        {
                            if(Vector3.Distance(transform.position, current.getTo().transform.position) < 1)
                            {
                                //we're there
                                status = Status.Unassigned;
                                current.getTo().addPerson(this);
                            }
                            else
                            {
                                Vector3 move = current.getTo().transform.position - transform.position;
                                transform.position += move.normalized * TimeManager.deltaTick * walkSpeed;
                            }
                        }
                    }
        }
        catch
        {
            //nothing
        }
        
    }

    public void init(string name, string type, int start)
    {
        Person.counter += 1;
        setId();
        this.setName(name);
        this.setType(type);
        this.setStart(start);
        this.setStatus(Person.Status.Unassigned);
    }

    void Update()
    {
        remainingTime -= Time.deltaTime;

        if (ClickManager.personBuffer == this) {
            spr.sprite = Resources.Load<Sprite>("Sprites/PersonSelected");
        } else {
            spr.sprite = Resources.Load<Sprite>("Sprites/Person");
        }

        if(remainingTime < 0)
        {
            float scale = transform.localScale.x;

            if(scale < 0.1f)
            {
                TimeManager.OnTick -= tick;

                if(current != null)
                {
                    if(current.getTo() != null)
                    {
                        current.getTo().removePerson(this);
                    }
                }

                TimeManager.decrementLives();
                Destroy(gameObject);
            }

            scale = Mathf.Lerp(scale, 0, 4 * Time.deltaTime);
            transform.localScale = new Vector3(scale, scale, 1);
        }

        if(Vector3.Distance(transform.position, Node.AirportLocation) < 4)
        {
            TimeManager.increasePoints();
            Destroy(gameObject);
            TimeManager.OnTick -= tick;
        }
    }

    public void PressPlayer()
    {
        spr.color = standardColour;
        // Debug.Log("PRESSED PLAYER");
        ClickManager.personBuffer = this;
        
        if(ClickManager.nodeBuffer == null)
        {
            if(status == Status.Moving)
            {
                status = Status.Departing;
                ClickManager.personBuffer = null;
            }
            else if(status == Status.Departing)
            {
                status = Status.Moving;
                ClickManager.personBuffer = null;
            }
            else if(status == Status.Waiting)
            {
                status = Status.Unassigned;
                ClickManager.personBuffer = null;
            }
            else if(status == Status.Unassigned)
            {
                status = Status.Waiting;
            }
        }
        else
        {
            ClickManager.pollStates();
        }
    }

    // Setters

    void setId()
    {
        this.id = Person.counter;
    }

    void setType(string type)
    {
        this.type = type;
    }

    void setName(string name)
    {
        this.personName = name;
    }

    void setStart(int start)
    {
        this.start = start;
    }

    public void setStatus(Person.Status status)
    {
        this.status = status;
    }

    public void setConnection(Connection c)
    {
        current = c;
        if(c.getIsAirPort() == 1)
        {
            // Debug.Log("I MADE IT");
        }
    }

    // Getters
    public Connection getCurrent()
    {
        return current;
    }

    public int getId()
    {
        return this.id;
    }

    public string getType()
    {
        return this.type;
    }

    public string getName()
    {
        return this.personName;
    }

    public int getStart()
    {
        return this.start;
    }

    public Person.Status getStatus()
    {
        return this.status;
    }
}
