﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantWalker : Walker
{
    [SerializeField]
    private int wait = 10;

    [SerializeField]
    private Vector2 peopleIconOffset;
    [SerializeField]
    private float peopleIconSpacing;

    private int waitedFor = 0;

    private bool hasPickedUp = false;

    // Start is called before the first frame update
    void Start()
    {
        if (wait == 0) { // should wait by default
            this.wait = 10;
        }
        TimeManager.OnTick += this.tick;

        gameObject.AddComponent<WalkerPositioning>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void init(Type type, float speed, Connection current)
    {
        this.type = type;
        this.setSpeed(speed);
        this.setConnection(current);
        this.setStatus(Walker.Status.WAITING);
        TimeManager.OnTick += this.tick;
    }

    public override void tick()
    {
        if (current == null)
            return;
        //Debug.Log(current.getFrom() + " to " + current.getTo());
        if (current.getTo() != null && progress < current.getLength())
        {
            PositionPlayers();
            this.progress += 0.025f * speed;
            hasPickedUp = false;
        }
        else
        { // at end of 
            if(current.getTo() != null)
            {
                if (current.getTo().getWait() && HasPeopleWaiting(current.getTo().getPeople()))
                { // this is a bus stop
                    wait = Mathf.Min(100, current.getTo().getPeople().Count * 20);
                    pickup();
                    hasPickedUp = true;
                }
            }


            RemovePeopleDeparting(people, current.getTo());

            if (hasPickedUp) {
                waitedFor++;

                if (waitedFor < wait) {
                    return;
                }
            }
            waitedFor = 0;
            progress = 0;
            goNext();
        }
    }

    // Pickup people from the bus stop and add them to the Walker object List
    private void pickup()
    {
        List<Person> peopleToRemove = new List<Person>();
        // pickup everyone at the stop
        foreach (Person p in current.getTo().getPeople())
        {
            if(p == null)
            {
                peopleToRemove.Add(p);
                continue;
            }

            if(p.getStatus() == Person.Status.Waiting)
            {
                addPerson(p);
                p.setStatus(Person.Status.Moving);
                peopleToRemove.Add(p);
            }

        }

        foreach (Person p in peopleToRemove)
        {
            current.getTo().getPeople().Remove(p);
        }
    }

    private void RemovePeopleDeparting(List<Person> people, Node n)
    {
        if(people.Count < 1 && n.getWait() )
            return;

        List<Person> peopleToRemove = new List<Person>();

        foreach(Person p in people)
        {
            if(p.getStatus() == Person.Status.Departing)
            {
                peopleToRemove.Add(p);
            }
        }

        foreach (Person p in peopleToRemove)
        {
            if(p != null)
                DepartPerson(n, p);
        }
    }

    private void DepartPerson(Node node, Person p)
    {
        p.setStatus(Person.Status.Unassigned);
        removePerson(p);
        node.addPerson(p);
        Connection tmpConnection = new Connection(null, node, 0, (Type)2, 0, 0);
        p.setConnection(tmpConnection);
    }

    private bool HasPeopleWaiting(List<Person> people)
    {
        foreach(Person p in people)
        {
            if(p.getStatus() == Person.Status.Waiting)
                return true;
        }

        return false;
    }

    private void goNext()
    {
        Node at = current.getTo();
        Connection walker = at.getConnections().Find(c => c.getType() == getType() /*&& c.getTo() != current.getFrom()*/);
        if (walker == null)
        {
            //Debug.Log("No next walker\n");
        }

        current = walker;
    }

    private void PositionPlayers()
    {
        int count = 0;

        List<Person> listToRemove = new List<Person>();

        foreach(Person p in people)
        {
            if(p == null)
            {
                listToRemove.Add(p);
                continue;
            }
            Vector3 Desired = transform.position + new Vector3(count * peopleIconSpacing, 0, 0) + new Vector3(peopleIconOffset.x, peopleIconOffset.y);
            p.transform.position = Desired;
            ++count;
        }

        foreach(Person p in listToRemove)
        {
            people.Remove(p);
        }
    }

    public void setOffsetSetting(Vector3 settings)
    {
        peopleIconOffset = new Vector2(settings.x, settings.y);
        peopleIconSpacing = settings.z;
    }
}
