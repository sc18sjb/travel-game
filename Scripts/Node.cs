﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    public static Vector3 AirportLocation;
    [SerializeField]
    private List<Walker> walkers = new List<Walker>();

    [SerializeField]
    private List<Connection> connections = new List<Connection>();

    // If a person is at a node, add them to the list to  
    [SerializeField]
    private List<Person> people = new List<Person>();

    [SerializeField]
    private bool wait;

    Vector2 coord;
    public int[] gridPos = new int[2];

    [SerializeField]
    private Vector2 peopleIconOffset;

    [SerializeField]
    private float peopleIconSpacing;

    [SerializeField]
    public int isAirport;

    Color hoverColour = Color.grey;
    Color standardColour;
    SpriteRenderer spr;


    // Start is called before the first frame update
    void Start()
    {
        BoxCollider2D bc = gameObject.AddComponent<BoxCollider2D>();
        bc.size = new Vector2(0.5f, 0.5f);
        // NaNi
        spr = gameObject.AddComponent<SpriteRenderer>();
        spr.sprite = Resources.Load<Sprite>("Sprites/Node"); 
        standardColour = spr.color;
        spr.sortingOrder = 1;
        this.transform.localScale = new Vector3(4, 4, 4);             
                                                           
            
        for (int i = 0; i < getConnections().Count; i++)              
        {                                                             
            if (getConnections()[i].getIsBusStop()==1)                     
            {
                if (getConnections()[i].getType() == Type.BUS)
                {
                    this.transform.localScale = new Vector3(7, 7, 7);
                    spr.sprite = Resources.Load<Sprite>("Sprites/BusStation");
                    wait = true;
                }
                    
                else if (getConnections()[i].getType() == Type.TRAIN)
                {
                    this.transform.localScale = new Vector3(7, 7, 7);
                    spr.sprite = Resources.Load<Sprite>("Sprites/TrainStation");
                    wait = true;
                }
            }
            else if (getConnections()[i].getIsAirPort() == 1)
            {
                this.transform.localScale = new Vector3(5, 5, 5);
                spr.sprite = Resources.Load<Sprite>("Sprites/Airport");
                spr.sortingOrder = 1;
                wait = true;
                Node.AirportLocation = transform.position;
            }
        }                                                             

        OnClickSpriteMessage clicker = gameObject.AddComponent<OnClickSpriteMessage>();
        clicker.setMessage("PressNode");

    }

    private void OnMouseOver()
    {
        spr.color = hoverColour;
    }

    private void OnMouseExit()
    {
        spr.color = standardColour;
    }

    public void PressNode()
    {
        Debug.Log("PRESS NODE");
        ClickManager.nodeBuffer = this;

        if(ClickManager.personBuffer != null)
        {
            ClickManager.pollStates();
        }
        else
        {
            if(people.Count > 0)
            {
                Person.Status s = people[0].getStatus() == Person.Status.Waiting? Person.Status.Unassigned : Person.Status.Waiting;
                foreach (Person p in people)
                {
                    p.setStatus(s);
                }
            }
        }
    }

    // setters 
    public void addWalker(Walker walker)
    {
        this.walkers.Add(walker);
    }

    public void setOffsetSetting(Vector3 settings)
    {
        peopleIconOffset = new Vector2(settings.x, settings.y);
        peopleIconSpacing = settings.z;
    }

    public void addConnection(Connection connection)
    {
        this.connections.Add(connection);
    }

    public void addPerson(Person person)
    {
        this.people.Add(person);
    }

    public void removePerson(Person person)
    {
        this.people.Remove(person);
    }

    public void setWait(bool wait)
    {
        this.wait = wait;
    }

    // gettes my man
    public List<Walker> getWalkers()
    {
        return this.walkers;
    }

    public List<Connection> getConnections()
    {
        return this.connections;
    }
    
    public List<Person> getPeople()
    {
        return this.people;
    }

    public bool getWait()
    {
        return this.wait;
    }

    // Update is called once per frame
    void Update()
    {
        if(people.Count > 0)
        {
            PositionPeople();
        }
    }

    private void PositionPeople()
    {
        foreach(Person p in people)
        {
            if (p.getStatus() == Person.Status.Walking)
                continue;

            int count = 0;
            p.transform.position = transform.position + new Vector3(peopleIconOffset.x + (count * peopleIconSpacing), peopleIconOffset.y);
            ++count;
        }
    }
}
