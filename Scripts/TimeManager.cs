﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeManager : MonoBehaviour
{
    //SUBSCRIBE BY USING
    //TimeManager.OnTick += yourFunctionName;
    //NO BRACKETS

    public static float deltaTick;
    [SerializeField]
    private float tickTime;
    [SerializeField]
    private float initialTickDelay;
    public delegate void Tick();
    public static event Tick OnTick;
    public static int remainingLives = 3;
    public static int score = 0;
    public GameObject LoseScreen;
    public GameObject WinScreen;

    void Awake()
    {
        TimeManager.deltaTick = tickTime;
    }

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("TickTime", initialTickDelay, tickTime);

        // RandomPerson();
    }

    private void TickTime()
    {
        if(OnTick != null)
        {
            OnTick();
        }

        if(TimeManager.remainingLives <= 0)
        {
            //you lose
            CancelInvoke("TickTime");
            LoseScreen.SetActive(true);
            Invoke("ReloadScene", 4);
        }

        if (TimeManager.score >= 8)
        {
            WinScreen.SetActive(true);
            CancelInvoke("TickTime");
            Invoke("ReloadScene", 4);
        }
    }

    public static void decrementLives()
    {
        TimeManager.remainingLives -= 1;
    }

    public static void increasePoints()
    {
        TimeManager.score += 1;
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }
}
