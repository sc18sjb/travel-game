﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class GridManager : MonoBehaviour
{
    string contents = "91,81,10,0,0,0\n81,71,10,0,0,0\n71,61,10,0,0,0\n61,60,10,0,1,0\n60,50,10,0,0,0\n50,40,10,0,0,0\n40,41,10,0,0,0\n41,42,10,0,0,0\n42,43,10,0,0,0\n43,53,10,0,0,0\n53,63,10,0,1,0\n63,73,10,0,0,0\n73,83,10,0,0,0\n83,84,10,0,0,0\n84,94,10,0,1,0\n94,93,10,0,0,0\n93,92,10,0,0,0\n92,91,10,0,1,0\n84,74,10,1,0,0\n74,75,10,1,0,0\n75,76,10,1,0,0\n76,77,10,1,1,0\n77,67,10,1,0,0\n67,68,10,1,0,0\n68,78,10,1,0,0\n78,89,10,1,0,0\n89,88,10,1,0,0\n88,87,10,1,1,0\n87,97,10,1,0,0\n97,96,10,1,0,0\n96,95,10,1,0,0\n95,85,10,1,0,0\n85,84,10,1,0,0\n66,65,10,1,0,0\n65,64,10,1,0,0\n64,54,10,1,1,0\n54,53,10,1,0,0\n53,52,10,1,0,0\n52,51,10,1,0,0\n51,41,10,1,0,0\n41,31,10,1,0,0\n31,32,10,1,0,0\n32,33,10,1,1,0\n33,23,10,1,0,0\n23,13,10,1,0,0\n13,3,10,1,0,0\n3,4,10,1,0,0\n4,5,10,1,1,0\n5,6,10,1,0,0\n6,16,10,1,0,0\n16,17,10,1,0,0\n17,18,10,1,0,0\n18,28,10,1,0,0\n28,38,10,1,1,0\n38,48,10,1,0,0\n48,58,10,1,0,0\n58,57,10,1,0,0\n57,56,10,1,1,0\n56,66,10,1,0,0\n56,55,10,0,0,0\n55,45,10,0,1,0\n45,35,10,0,0,0\n35,34,10,0,0,0\n34,24,10,0,0,0\n24,14,10,0,0,0\n14,15,10,0,0,0\n15,25,10,0,0,0\n25,26,10,0,0,0\n26,27,10,0,1,0\n27,17,10,0,0,0\n17,7,10,0,0,0\n7,8,10,0,0,0\n8,9,10,0,1,0\n9,19,10,0,0,1\n19,29,10,0,0,0\n29,39,10,0,0,0\n39,49,10,0,1,0\n49,48,10,0,0,0\n48,47,10,0,0,0\n47,46,10,0,0,0\n46,56,10,0,0,0\n30,31,10,0,0,0\n31,21,10,0,1,0\n21,22,10,0,0,0\n22,12,10,0,0,0\n12,2,10,0,0,0\n2,1,10,0,0,0\n1,0,10,0,1,0\n0,10,10,0,0,0\n10,20,10,0,0,0\n20,30,10,0,1,0\n# TRANSPORT\n0,2,75\n0,2,61\n1,3,47\n1,3,37\n0,2,1\n1,3,20\n0,2,88";
    // public GameObject gridManager;

    //public var dataValues;
    public static List<Node> grid = new List<Node>();
    public static List<Connection> cons = new List<Connection>();
    public static List<Connection> cons2 = new List<Connection>();
    public List<Person> people = new List<Person>();
    public List<GameObject> hearts = new List<GameObject>();
    private List<string> names = new List<string>()
    {
        "Bazinga",
        "Leeds is Better",
        "Great Uni Hack",
        "8 Octopus Recipes",
        "Not Hotdog",
        "Simba (from Lion King™)",
        "Spooky Skeletons",
        "Elliot Alderson",
        "Bertram Gilfoyle",
        "Mr. Peanutbutter",
        "Ron LaFlamme",
        "Todd Chavez",
        "Gavin Belson",
        "bee",
        "Phoebe Bridgers",
        "King Giz",
        "Tiny Meat Gang"
    };

    public int num = 0;

    private int count = 0;
    public int heartCount = 3;

    public Color[] lineColours;
    public float lineThicknesses;

    [SerializeField]
    private Vector2 spacing = new Vector2(5,5);

    public Text ScoreText;
    public Text PeopleText;
    public Canvas canvas;

    private float timer;
    private int score;

    void Start() {
        TimeManager.OnTick += this.tick;
        AddPerson();
        for (int i = 0; i < TimeManager.remainingLives; i++)
        {
            GameObject imgObj = new GameObject();
            imgObj.transform.parent = canvas.transform;
            Image img = imgObj.AddComponent<Image>();
            img.sprite = Resources.Load<Sprite>("Sprites/Heart");
            img.transform.position = new Vector3(i * 60 + 65, 40);
            img.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            hearts.Add(img.gameObject);
        }
    }

    private void RandomPerson() {
        float delay = Random.Range(3, 10);

 

        //Invoke("RandomPerson", Random.Range(3, 10));
    }

    private void AddPerson() {
        GameObject p = new GameObject("Person" + Person.counter);
        Person a = p.AddComponent<Person>();
        a.init(getName(), "ceo", score); // score is current second
        a.setStatus(Person.Status.Unassigned);
        a.setConnection(cons[Random.Range(0, cons.Count)]);
        p.transform.position = a.getCurrent().getTo().gameObject.transform.position + new Vector3(-2, 2, 0);
        
        SpriteRenderer sr = p.AddComponent<SpriteRenderer>();
        sr.sprite = Resources.Load<Sprite>("Sprites/Person");
        sr.transform.localScale = new Vector3(6, 6, 6);
        sr.sortingOrder = 1;

        addPerson(a);
    }

    public void addPerson(Person person)
    {
        this.people.Add(person);
        this.names.Remove(person.getName());
    }

    public void removePerson(Person person)
    {
        this.people.Remove(person);
    }

    public List<Person> getPeople()
    {
        return this.people;
    }

    public string getName()
    {
        int r = Random.Range(0, this.names.Count);
        return this.names[r];
    }

    public void tick()
    {
        count++;
        PeopleText.text = "";

        for (int i = getPeople().Count - 1; i >= 0; i--) {
            if (getPeople()[i] == null) {
                removePerson(getPeople()[i]);
            }
        }

        foreach(Person p in getPeople())
        {

            if (ClickManager.personBuffer == p)
            {
                PeopleText.text += "<color=green>" + p.getName() + "</color>\n";
                PeopleText.text += "    <color=green>➜ Status: " + p.getStatus().ToString() + "</color>\n";
                PeopleText.text += "    <color=green>➜ Time To Flight: " + p.getTimeRemain() + "</color>\n\n";
            }
            else
            {
                PeopleText.text += p.getName() + "\n";
                PeopleText.text += "    ➜ Status: " + p.getStatus().ToString() + "\n";
                PeopleText.text += "    ➜ Time To Flight: " + p.getTimeRemain() + "\n\n";
            }
        }

        if (TimeManager.remainingLives < heartCount)
        {
            heartCount = TimeManager.remainingLives;
            hearts[heartCount].SetActive(false);
        }
        

        if (count % 500 == 0 && people.Count < 5)
        {
            AddPerson();
        }
    }

    void Update()
    {

        timer += Time.deltaTime;

        if (timer > 1f)
        {

            score += 1;

            //We only need to update the text if the score changed.
             ScoreText.text = "Time: " + score.ToString();

            //Reset the timer to 0.
            timer = 0;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        CreateNodes();
        CreateConnections();
        AddTransport();
    }

    void CreateNodes()
    {
        int numNodes = countNodes();
        int row = 0;
        int total = num * num;
        for (int i = 0; i < total; i++)
        {
            GameObject newNode = new GameObject("Node" + i);
            Node n = newNode.AddComponent<Node>();
            n.setOffsetSetting(new Vector3(-2, 2, 2));

            GridManager.grid.Add(n);

            newNode.transform.position = new Vector3(i % num * spacing.x, row * spacing.y, 0);

            if (i%num == num - 1)
            {
                ++row;
            }
        }
    }

    int countNodes()
    {
        int counter = 0;
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(contents);
        MemoryStream ms = new MemoryStream(bytes);
        StreamReader streamReader = new StreamReader(ms);
        // StreamReader streamReader = new StreamReader(Application.dataPath+"/Resources/file.csv");
        bool EOF = false;
        while (!EOF)
        {

            string dataString = streamReader.ReadLine();
            if (dataString == "# TRANSPORT")
            {
                EOF = true;
                break;
            }
            counter++;
        }
        return counter;
    }

    // Update is called once per frame
    void CreateConnections()
    {
        int countNodes = 0;
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(contents);
        MemoryStream ms = new MemoryStream(bytes);
        StreamReader streamReader = new StreamReader(ms);
        // StreamReader streamReader = new StreamReader(Application.dataPath+"/Resources/file.csv");
        bool EOF = false;
        while (!EOF)
        {
            string dataString = streamReader.ReadLine();
            if (dataString == "# TRANSPORT")
            {
                EOF = true;
                break;
            }
            countNodes++;
            //can access by inde

            string[] dataValues = new string[5];
            //for (int i = 0; i < 5; i++) {
                dataValues = dataString.Split(',');
                
                int from = int.Parse(dataValues[0]);
                int to = int.Parse(dataValues[1]);
                int length = int.Parse(dataValues[2]);
                int transportType = int.Parse(dataValues[3]);
                int isBusStop = int.Parse(dataValues[4]);
                int isAirPort = int.Parse(dataValues[5]);
                Type t = (Type)transportType;
                
                Connection con1 = new Connection(GridManager.grid[from], GridManager.grid[to], length, t, isBusStop, isAirPort);
                Connection con2 = new Connection(GridManager.grid[to], GridManager.grid[from], length, t, isBusStop, isAirPort);
                GridManager.grid[from].addConnection(con1);
                GridManager.grid[from].addConnection(con2);
                cons.Add(con1); // used for AddTransport
                //Debug.Log("index: "+1+" "+con1);


                DrawConnection drawer = DrawConnection.CreateNewLineGraphic(lineColours[transportType], lineThicknesses, con1);
                drawer.setTransportType(transportType);
            //}
        }
    }

    void AddTransport() // type, speed, ref to current
    {
        int counter = 0;
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(contents);
        MemoryStream ms = new MemoryStream(bytes);
        StreamReader streamReader = new StreamReader(ms);
        // StreamReader streamReader = new StreamReader(Application.dataPath+"/Resources/file.csv");
        bool sawHeader = false;
        bool EOF = false;
        while (!EOF)
        {
            string dataString = streamReader.ReadLine();
            if (dataString == "# TRANSPORT")
            {
                sawHeader = true;
                continue;
            }
            if (dataString == null)
            {
                EOF = true;
                break;
            }
            if (sawHeader) {
                string[] dataValues = dataString.Split(',');
                Type t = (Type)int.Parse(dataValues[0]);
                int s = int.Parse(dataValues[1]);
                int conIndex = int.Parse(dataValues[2]);

                GameObject go = new GameObject("Transport" + counter);
                ConstantWalker w = go.AddComponent<ConstantWalker>();
                w.init(t, s, cons[conIndex]);
                w.setOffsetSetting(new Vector3(-2, 2, 2));

                if (w.getType() == Type.BUS)
                {
                    SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
                    sr.sprite = Resources.Load<Sprite>("Sprites/Bus");
                    sr.sortingOrder = 2;
                }
                else if (w.getType() == Type.TRAIN)
                {
                    SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
                    sr.sprite = Resources.Load<Sprite>("Sprites/Train");
                    sr.sortingOrder = 2;
                } 
                         
                go.transform.localScale = new Vector3(7, 7, 7);

                counter++;
            }
        }
    }
}
