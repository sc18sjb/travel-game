﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkerPositioning : MonoBehaviour
{
    private Walker walker;
    // Start is called before the first frame update
    void Start()
    {
        walker = GetComponent<Walker>();
    }

    private void Update()
    {
        if(walker != null)
        {
            Connection con = walker.getConnection();
            if (con != null)
            {
                if(con.getTo() != null && con.getFrom() != null)
                    PositionWalker(con, walker.getProgress(), con.getLength());
            }
        }
    }

    void PositionWalker(Connection con, float progress, int max)
    {
        Vector3 from = con.getFrom().transform.position;
        Vector3 to = con.getTo().transform.position;

        Vector3 directionalVector = to - from;

        directionalVector = directionalVector * (progress / max);

        transform.position = from + directionalVector;
    }
}
