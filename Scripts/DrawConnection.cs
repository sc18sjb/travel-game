﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawConnection : MonoBehaviour
{
    public Color lineColour;
    public float lineThickness;

    public GameObject startObject;
    public GameObject endObject;

    LineRenderer line;

    public bool autoInitialise;

    private int transportType = -1;

    private void Start()
    {
        if (!autoInitialise)
            return;

        Vector3[] points = new Vector3[2];
        points[0] = startObject.transform.position;
        points[1] = endObject.transform.position;

        Initialise(lineColour, lineThickness, points);
    }

    public void setTransportType(int t)
    {
        transportType = t;
    }

    public void CullLayer(int exposedLayer)
    {
        if(transportType == -1 || transportType == exposedLayer)
        {
            line.enabled = true;
        }
        else
        {
            line.enabled = false;
        }
    }

    public void Initialise(Color colour, float thickness, GameObject start, GameObject end)
    {
        Vector3[] points = new Vector3[2];
        points[0] = start.transform.position;
        points[1] = end.transform.position;

        Initialise(colour, thickness, points);
    }

    // Start is called before the first frame update
    public void Initialise(Color colour, float thickness, Vector3[] points)
    {
        line = gameObject.AddComponent<LineRenderer>();

        line.startColor = colour;
        line.endColor = colour;

        line.startWidth = thickness;
        line.endWidth = thickness;

        line.material = new Material(Shader.Find("Legacy Shaders/Particles/Alpha Blended"));

        if(points.Length > 0)
            line.SetPositions(points);
    }

    public void Initialise(Color colour, float thickness, Connection connection)
    {
        Initialise(colour, thickness, connection.getFrom().gameObject, connection.getTo().gameObject);
    }

    public void DrawLine(Vector2 pos1, Vector2 pos2)
    {
        Vector3 startPoint = new Vector3(pos1.x, pos1.y);
        Vector3 endPoint = new Vector3(pos2.x, pos2.y);
        DrawLine(startPoint, endPoint);
    }

    public void DrawLine(GameObject pos1, GameObject pos2)
    {
        DrawLine(pos1.transform.position, pos2.transform.position);
    }

    public void DrawLine(Vector3 pos1, Vector3 pos2)
    {
        Vector3[] points = new Vector3[2];
        points[0] = pos1;
        points[1] = pos2;

        line.SetPositions(points);
    }

    public void DrawLine(Connection connection)
    {
        DrawLine(connection.getFrom().gameObject, connection.getTo().gameObject);
    }

    public static DrawConnection CreateNewLineGraphic(Color color, float thickness, Connection connection)
    {
        GameObject newLineRenderer = new GameObject("LineDraw");
        DrawConnection drawer = newLineRenderer.AddComponent<DrawConnection>();

        drawer.Initialise(color, thickness, connection);
        return drawer;
    }
}
